// 1653043_Project.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "1653043_Project.h"

#define MAX_LOADSTRING      100
#define MDI_ID_FIRST_CHILD  50000       // ID of MDI client's first child
#define ID_TOOLBAR          1000	    // ID of toolbar
#define IMAGE_WIDTH         18
#define IMAGE_HEIGHT        17
#define BUTTON_WIDTH        0
#define BUTTON_HEIGHT       0
#define TOOLBAR_HEIGHT		28
#define TOOL_TIP_MAX_LEN    32

// Global Variables:
HINSTANCE hInst;                        // current instance
WCHAR szTitle[MAX_LOADSTRING];          // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];    // the main window class name
HWND hWndMDIClient = NULL;
HWND hWndMDIFrame = NULL;
HWND hToolbarWnd = NULL;
bool DrawButtonsAreDrawn = false;
bool EditButtonsAreDrawn = false;
int ChildrenNo = 0;
int orgX = NULL, orgY = NULL;
bool resizing = FALSE;
int targetAnchor = -1;

// Forward declarations of functions included in this code module:
ATOM                RegisterMDIFrameClass(HINSTANCE);
ATOM                RegisterMDI_DrawChildClass(HINSTANCE);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    FrameWndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK    DrawWndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    InputText(HWND, UINT, WPARAM, LPARAM);
VOID                initMDIFrame(HWND);
VOID                MDIActivate(HWND);
VOID                CreateDrawChild(HWND);
VOID                SelectDrawMode(HWND, int);
LRESULT CALLBACK    MDICloseProc(HWND, LPARAM);
VOID                CreateToolbar(HWND);
VOID                Toolbar_AddDrawButtons();
VOID				Toolbar_AddEditButtons();
VOID                Toolbar_NotifyHandle(LPARAM);
VOID                Toolbar_RemoveDrawButtons();
VOID				ShowColorDialog(HWND);
VOID				ShowFontDialog(HWND);
VOID                ShowOpenDialog(HWND);
VOID                ShowSaveDialog(HWND);
BOOL                OpenDrawingFile(HWND, LPCTSTR);
BOOL                SaveDrawingFile(HWND, LPCTSTR);
int                 GetSelectedObject(MDI_CHILD_DATA* _WndData);
VOID                onCut(HWND);
VOID                onCopy(HWND);
VOID                onPaste(HWND);
VOID                onDelete(HWND);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_MY1653043_PROJECT, szWindowClass, MAX_LOADSTRING);
    
    RegisterMDIFrameClass(hInstance);
	RegisterMDI_DrawChildClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MY1653043_PROJECT));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateMDISysAccel(hWndMDIClient, &msg) &&
			!TranslateAccelerator(hWndMDIFrame, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM RegisterMDIFrameClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = FrameWndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MY1653043_PROJECT));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_MY1653043_PROJECT);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}

ATOM RegisterMDI_DrawChildClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = DrawWndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 8;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MY1653043_PROJECT));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = L"MDI_DRAW_CHILD";
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK FrameWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_CREATE:
		initMDIFrame(hWnd);
		break;
    case WM_SIZE:
		{
			UINT width, height;
			width = LOWORD(lParam);
			height = HIWORD(lParam);
			MoveWindow(hWndMDIClient, 0, TOOLBAR_HEIGHT, width, height, TRUE);
		}
		return DefWindowProc(hWnd, message, wParam, lParam);
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
			switch (wmId)
			{
			case ID_FILE_NEW:
				CreateDrawChild(hWndMDIClient);
				if (!DrawButtonsAreDrawn) Toolbar_AddDrawButtons();
				if (!EditButtonsAreDrawn) Toolbar_AddEditButtons();
				break;
			case ID_FILE_OPEN:
			{
				// Get current active child window
				HWND hActiveChild = (HWND)SendMessage(hWndMDIClient, WM_MDIGETACTIVE, 0, 0);
				ShowOpenDialog(hActiveChild);
				InvalidateRect(hActiveChild, nullptr, FALSE);
			}
			break;
			case ID_FILE_SAVE:
			{
				// Get current active child window
				HWND hActiveChild = (HWND)SendMessage(hWndMDIClient, WM_MDIGETACTIVE, 0, 0);
				ShowSaveDialog(hActiveChild);
			}
			break;
			case ID_DRAW_COLOR:
				// TODO: Open "Color" common dialog box
				ShowColorDialog(hWnd);
				break;
			case ID_DRAW_FONT:
				// TODO: Open "Font" common dialog box
				ShowFontDialog(hWnd);
				break;
			case ID_DRAW_LINE:      case ID_DRAW_RECTANGLE:
			case ID_DRAW_ELLIPSE:   case ID_DRAW_TEXT:      case ID_DRAW_SELECTOBJECT:
				SelectDrawMode(hWnd, wmId);
				break;
			case ID_EDIT_CUT:
				{
					onCut(hWnd);					
				}
				break;
			case ID_EDIT_COPY:
				{
					onCopy(hWnd);					
				}
				break;
            case ID_EDIT_PASTE:
                {
					onPaste(hWnd);
				}
                break;
            case ID_EDIT_DELETE:
				{
					/*HWND hActiveChild = (HWND)SendMessage(hWndMDIClient, WM_MDIGETACTIVE, 0, 0);
					MDI_CHILD_DATA* _WndData = (MDI_CHILD_DATA*)GetWindowLongPtrW(hActiveChild, 0);
					std::vector<Shape*> tmp = _WndData->ShapeArr;
					tmp.erase(tmp.begin());
					std::swap(tmp, _WndData->ShapeArr);
					InvalidateRect(hActiveChild, nullptr, 0);*/
					//_WndData->ShapeArr.erase(_WndData->ShapeArr.begin());
					onDelete(hWnd);
				}
                break;
            case ID_WINDOW_TILE:
                // Tile horizontally
                SendMessage(hWndMDIClient, WM_MDITILE, MDITILE_HORIZONTAL, 0L);
				break;
            case ID_WINDOW_CASCADE:
				SendMessage(hWndMDIClient, WM_MDICASCADE, 0, 0);
				break;
            case ID_WINDOW_CLOSEALL:
				EnumChildWindows(hWndMDIClient, (WNDENUMPROC) MDICloseProc, 0L);
				break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefFrameProc(hWnd, hWndMDIClient, message, wParam, lParam);
            }
        }
        return DefFrameProc(hWnd, hWndMDIClient, message, wParam, lParam);
    case WM_NOTIFY:
		Toolbar_NotifyHandle(lParam);
		return 0;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

LRESULT CALLBACK DrawWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
    case WM_CREATE:
        {
            // Initialize window data
            MDI_CHILD_DATA* _WndData =
                (MDI_CHILD_DATA*) VirtualAlloc(NULL, sizeof(MDI_CHILD_DATA),
                MEM_COMMIT, PAGE_READWRITE);
            // Set initial values
            HDC hdc = GetDC(hWnd);
            HGDIOBJ hGDIObj;
			_WndData->hWnd = hWnd;
			_WndData->_color = RGB(0, 0, 0);
            hGDIObj = GetCurrentObject(hdc, OBJ_FONT);
            GetObject(hGDIObj, sizeof(LOGFONT), &(_WndData->_font));
            _WndData->crsType = Normal;
			_WndData->nShapes = 0;
			_WndData->DrawCurrTextBox = FALSE;
            ReleaseDC(hWnd, hdc);

            SetLastError(0);
            // Save data into window's extra bytes
            if (SetWindowLongPtrW(hWnd, 0, (LONG_PTR) _WndData) == 0) {
                if (GetLastError() != 0) {
                    // TODO: Handle SetWindowLongPtrW failure

                }
            }
            return 0;
        }
	case WM_MDIACTIVATE:
		MDIActivate(hWnd);
		break;
	case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            RECT r;
            GetClientRect(hWnd, &r);
            int width = r.right, height = r.bottom;

            HDC bufferDC = CreateCompatibleDC(hdc);
            HBITMAP bufferBmp = CreateCompatibleBitmap(hdc, width, height);

            int savedDC = SaveDC(bufferDC);
            SelectObject(bufferDC, bufferBmp);
            HBRUSH hBr = CreateSolidBrush(RGB(255,255,255));
            FillRect(bufferDC, &r, hBr);

			// Paint loop
			MDI_CHILD_DATA* _WndData = (MDI_CHILD_DATA*)GetWindowLongPtrW(hWnd, 0);
			for (int i = 0; i < _WndData->nShapes; i++) {
				if (i == _WndData->nShapes - 1 && _WndData->DrawCurrTextBox) {
					_WndData->ShapeArr[_WndData->nShapes - 1]->DrawTextBox(bufferDC);
				}
				else {
					_WndData->ShapeArr[i]->Draw(bufferDC);
					if (_WndData->ShapeArr[i]->hoverOn) {
						_WndData->ShapeArr[i]->cueSelection(bufferDC);
					}
					if (_WndData->ShapeArr[i]->selected) {
						_WndData->ShapeArr[i]->displaySelection(bufferDC);
					}
				}
			}

            BitBlt(hdc, 0, 0, width, height, bufferDC, 0, 0, SRCCOPY);
            RestoreDC(bufferDC, savedDC);

            DeleteObject(bufferBmp);
            DeleteDC(bufferDC);
			
            // TODO: Implement a data structure to store drawn objects
            //		and rewrite WM_PAINT to redraw objects
            EndPaint(hWnd, &ps);
			return 0;
        }
    	break;
    case WM_LBUTTONDOWN:
		{
            int mouseX = orgX = LOWORD(lParam);
            int mouseY = orgY = HIWORD(lParam);

			MDI_CHILD_DATA* _WndData = (MDI_CHILD_DATA*)GetWindowLongPtrW(hWnd, 0);
			CursorType cType = _WndData->crsType;
			switch (cType) {
			case DrawLine:
				{
					_WndData->nShapes++;
					_WndData->ShapeArr.push_back((Line*) new Line);
					_WndData->ShapeArr.back()->SetStartPoint(mouseX, mouseY);
					_WndData->ShapeArr.back()->SetEndPoint(mouseX, mouseY);
					_WndData->ShapeArr.back()->SetColor(_WndData->_color);
				}
				break;
			case DrawRect:
				{
					_WndData->nShapes++;
					_WndData->ShapeArr.push_back((Rect*) new Rect);
					_WndData->ShapeArr.back()->SetStartPoint(mouseX, mouseY);
					_WndData->ShapeArr.back()->SetEndPoint(mouseX, mouseY);
					_WndData->ShapeArr.back()->SetColor(_WndData->_color);
				}
				break;
			case DrawElps:
				{
					_WndData->nShapes++;
					_WndData->ShapeArr.push_back((Elps*) new Elps);
					_WndData->ShapeArr.back()->SetStartPoint(mouseX, mouseY);
					_WndData->ShapeArr.back()->SetEndPoint(mouseX, mouseY);
					_WndData->ShapeArr.back()->SetColor(_WndData->_color);
				}
				break;
			case DrawTextObj:
				{
					_WndData->nShapes++;
					_WndData->ShapeArr.push_back((TextObj*) new TextObj);
					_WndData->ShapeArr.back()->SetStartPoint(mouseX, mouseY);
					_WndData->ShapeArr.back()->SetEndPoint(mouseX, mouseY);
					_WndData->ShapeArr.back()->SetColor(_WndData->_color);
					_WndData->ShapeArr.back()->SetFont(_WndData->_font);
				}
            case SelectObj:
                {					
                    for (int i = _WndData->nShapes - 1; i >= 0; --i) {						
						if (_WndData->ShapeArr[i]->hoverOn) {
							for (int i = 0; i < _WndData->nShapes; ++i) {
								_WndData->ShapeArr[i]->selected = FALSE;
							}
							_WndData->ShapeArr[i]->selected = TRUE;
							break;
						}
                    }
					InvalidateRect(hWnd, nullptr, FALSE);
                }
			}
		}
		break;
    case WM_MOUSEMOVE:
		{
            int mouseX = LOWORD(lParam);
            int mouseY = HIWORD(lParam);

			if (wParam == MK_LBUTTON) {
                RECT r;
                GetClientRect(hWnd, &r);
				ClientToScreen(hWnd, (POINT *)&r);
				ClientToScreen(hWnd, (POINT *)&r + 1);
                ClipCursor(&r);
				MDI_CHILD_DATA* _WndData = (MDI_CHILD_DATA*)GetWindowLongPtrW(hWnd, 0);
				CursorType cType = _WndData->crsType;
				switch (cType) {
				case DrawLine: case DrawRect: case DrawElps:
					{
						_WndData->ShapeArr.back()->SetEndPoint(mouseX, mouseY);
						InvalidateRect(hWnd, nullptr, FALSE);
					}
					break;
                case DrawTextObj:
                    {
                        _WndData->ShapeArr.back()->SetEndPoint(mouseX, mouseY);						
						_WndData->DrawCurrTextBox = TRUE;
						InvalidateRect(hWnd, nullptr, FALSE);
                    }
                    break;
				case SelectObj:
					{
                        int distX = mouseX - orgX;
						int distY = mouseY - orgY;                        
						for (int i = 0; i < _WndData->nShapes; i++) {
							if (_WndData->ShapeArr[i]->selected) {
                                int AnchorIdx = _WndData->ShapeArr[i]->onAnchor(orgX, orgY);
                                if (AnchorIdx != -1) {
									resizing = TRUE;
									targetAnchor = AnchorIdx;
                                }
								if (!resizing) {
									_WndData->ShapeArr[i]->Move(distX, distY);
								}
								else {
									_WndData->ShapeArr[i]->Resize(distX, distY, targetAnchor);
								}
							}
						}
                        orgX = mouseX;
						orgY = mouseY;
						InvalidateRect(hWnd, nullptr, FALSE);
					}
					break;
                default:
                    break;
				}
			}
            else {
                MDI_CHILD_DATA* _WndData = (MDI_CHILD_DATA*)GetWindowLongPtrW(hWnd, 0);
				CursorType cType = _WndData->crsType;
                switch (cType) {
                case SelectObj:
                    {
                        for (int i = _WndData->nShapes - 1; i >= 0; --i) {
                            if (_WndData->ShapeArr[i]->mouseOver(mouseX, mouseY)/* && !(_WndData->ShapeArr[i]->selected)*/) {
								for (int i = 0; i < _WndData->nShapes; ++i) {
									_WndData->ShapeArr[i]->hoverOn = FALSE;
								}
								_WndData->ShapeArr[i]->hoverOn = TRUE;
								InvalidateRect(hWnd, nullptr, FALSE);
								break;
                            }
							else {
								_WndData->ShapeArr[i]->hoverOn = FALSE;
								InvalidateRect(hWnd, nullptr, FALSE);
							}
                        }
                    }
                    break;
                default:
                    break;
                }
            }
		}
		break;
    case WM_LBUTTONUP:
        {
            RECT r;
            GetWindowRect(HWND_DESKTOP, &r);
            ClipCursor(&r);

			if (resizing) {
				resizing = FALSE;
				targetAnchor = -1;
			}

            MDI_CHILD_DATA* _WndData = (MDI_CHILD_DATA*)GetWindowLongPtrW(hWnd, 0);
            CursorType cType = _WndData->crsType;
            switch(cType) {
            case DrawTextObj:
                {
					_WndData->DrawCurrTextBox = FALSE;					
                    // TODO: get text input from dialog
					INT_PTR returnPtr = DialogBox(hInst, MAKEINTRESOURCEW(IDD_DIALOG1), hWnd, InputText);
					if (returnPtr != IDCLOSE) {
						WCHAR* buf = (WCHAR*)returnPtr;
						_WndData->ShapeArr.back()->SetText(buf);
						delete (WCHAR*)returnPtr;
					}
					else {
						_WndData->ShapeArr.pop_back();
						_WndData->nShapes--;
					}
					InvalidateRect(hWnd, nullptr, FALSE);
                }				
                break;				
            }
        }
		break;
	case WM_DESTROY:        
		DestroyWindow(hWnd);
		if (--ChildrenNo < 1) // All draw windows closed
		{
			// Remove draw buttons
            Toolbar_RemoveDrawButtons();
            DrawButtonsAreDrawn = false;
            EditButtonsAreDrawn = false;
			// Disable Draw menu
			HMENU hMenu = GetMenu(hWndMDIFrame);
			EnableMenuItem(hMenu, 1, MF_DISABLED | MF_BYPOSITION);
			DrawMenuBar(hWndMDIFrame);
		}
		break;
    // TODO: Process WM_MOUSEMOVE, WM_LBUTTONDOWN and WM_LBUTTONUP messages
    //      to enable drawing
	default:
		return DefMDIChildProc(hWnd, message, wParam, lParam);
	}
}

void initMDIFrame(HWND hWnd)
{
	// Store frame handler into a global variable
	//		for further use
	hWndMDIFrame = hWnd;

	// Initialize MDI client 
	CLIENTCREATESTRUCT ccs;
	ccs.hWindowMenu = GetSubMenu(GetMenu(hWnd), 2);
	ccs.idFirstChild = MDI_ID_FIRST_CHILD;
	hWndMDIClient = CreateWindowW(L"MDICLIENT", (LPCTSTR) NULL, WS_CHILD | WS_CLIPCHILDREN,
		CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, hWnd, (HMENU) NULL, hInst, (LPVOID) &ccs);
	ShowWindow(hWndMDIClient, SW_SHOW);

    // Disable "Draw" by default because no Draw child window is active
    HMENU hMenu = GetMenu(hWndMDIFrame);
    EnableMenuItem(hMenu, 1, MF_DISABLED | MF_BYPOSITION);

    CreateToolbar(hWnd);
}

void MDIActivate(HWND hWnd)
{
    // Enable "Draw" menu, check "Line" item and redraw menu bar
	HMENU hMenu = GetMenu(hWndMDIFrame);
	EnableMenuItem(hMenu, 1, MF_ENABLED | MF_BYPOSITION);	
	CheckMenuItem(GetSubMenu(hMenu, 1), ID_DRAW_LINE, MF_CHECKED);
	DrawMenuBar(hWndMDIFrame);

    // Recover window data
    MDI_CHILD_DATA* _WndData = (MDI_CHILD_DATA*) GetWindowLongPtrW(hWnd, 0);
    HDC hdc = GetDC(hWnd);
    SetDCBrushColor(hdc, _WndData->_color);
    HFONT hFont = CreateFontIndirectW(&(_WndData->_font));
    SelectObject(hdc, hFont);
    ReleaseDC(hWnd, hdc);
}

void CreateDrawChild(HWND hWndMDIClient)
{
    // Title of created child
    WCHAR WndTitle[10];
    static int DrawNo = 0;
    wsprintf(WndTitle, L"Noname-%d", ++DrawNo); ++ChildrenNo;

    // Create child window
	MDICREATESTRUCT mdiCreate;
	ZeroMemory(&mdiCreate, sizeof(MDICREATESTRUCT));   

	mdiCreate.szClass = L"MDI_DRAW_CHILD";
	mdiCreate.hOwner = hInst;
	mdiCreate.szTitle = WndTitle;
	mdiCreate.x = CW_USEDEFAULT;
	mdiCreate.y = CW_USEDEFAULT;
	mdiCreate.cx = CW_USEDEFAULT;
	mdiCreate.cy = CW_USEDEFAULT;
	mdiCreate.style = 0;
	mdiCreate.lParam = NULL;
	SendMessage(hWndMDIClient, WM_MDICREATE, (WPARAM) 0, (LPARAM) (LPMDICREATESTRUCT) &mdiCreate);
}

void SelectDrawMode(HWND hWnd, int wmId)
{
    HMENU hMenu = GetSubMenu(GetMenu(hWnd), 1);
    // Uncheck all draw modes
    for (int i = 2; i < 7; i++) {
        CheckMenuItem(hMenu, i, MF_UNCHECKED | MF_BYPOSITION);
    }
    // Check chosen item
    CheckMenuItem(hMenu, wmId, MF_CHECKED | MF_BYCOMMAND);

	// Get current active child window
	HWND hActiveChild = (HWND)SendMessage(hWndMDIClient, WM_MDIGETACTIVE, 0, 0);
    MDI_CHILD_DATA* _WndData = (MDI_CHILD_DATA*) GetWindowLongPtrW(hActiveChild, 0);

    switch (wmId)
    {
    case ID_DRAW_LINE:       
        _WndData->crsType = DrawLine;
		for (int i = 0; i < _WndData->nShapes; i++) {
			_WndData->ShapeArr[i]->selected = FALSE;
			_WndData->ShapeArr[i]->hoverOn = FALSE;
		}
        break;
    case ID_DRAW_RECTANGLE:
		for (int i = 0; i < _WndData->nShapes; i++) {
			_WndData->ShapeArr[i]->selected = FALSE;
			_WndData->ShapeArr[i]->hoverOn = FALSE;
		}
        _WndData->crsType = DrawRect;
        break;
    case ID_DRAW_ELLIPSE:
		for (int i = 0; i < _WndData->nShapes; i++) {
			_WndData->ShapeArr[i]->selected = FALSE;
			_WndData->ShapeArr[i]->hoverOn = FALSE;
		}
        _WndData->crsType = DrawElps;
        break;
    case ID_DRAW_TEXT:
		for (int i = 0; i < _WndData->nShapes; i++) {
			_WndData->ShapeArr[i]->selected = FALSE;
			_WndData->ShapeArr[i]->hoverOn = FALSE;
		}
        _WndData->crsType = DrawTextObj;
        break;
    case ID_DRAW_SELECTOBJECT:
        _WndData->crsType = SelectObj;
        break;
    }

	InvalidateRect(hActiveChild, nullptr, FALSE);
}

LRESULT CALLBACK MDICloseProc(HWND hChildWnd, LPARAM lParam)
{
	SendMessage(hWndMDIClient, WM_MDIDESTROY, (WPARAM) hChildWnd, 0L);
	return 1;
}

void CreateToolbar(HWND hWnd)
{
    // load Common Control DLL
	InitCommonControls();
	TBBUTTON tbButtons[] =
	{
		// Zero-based Bitmap image, ID of command, Button state, Button style, 
		//      App data, Zero-based string (Button's label)
		{ STD_FILENEW,	ID_FILE_NEW,            TBSTATE_ENABLED,    TBSTYLE_BUTTON, 0,  0 },
		{ STD_FILEOPEN,	ID_FILE_OPEN,           TBSTATE_ENABLED,    TBSTYLE_BUTTON, 0,  0 },
		{ STD_FILESAVE,	ID_FILE_SAVE,           TBSTATE_ENABLED,    TBSTYLE_BUTTON, 0,  0 },
        { 0,            0,                      TBSTATE_ENABLED,    TBSTYLE_SEP,    0,  0 }
	};	

	// Create toolbar
	hToolbarWnd = CreateToolbarEx(hWnd,	WS_CHILD | WS_VISIBLE |
            CCS_ADJUSTABLE | TBSTYLE_TOOLTIPS, ID_TOOLBAR,
            sizeof(tbButtons) / sizeof(TBBUTTON), HINST_COMMCTRL,
            0, tbButtons, sizeof(tbButtons) / sizeof(TBBUTTON),
            BUTTON_WIDTH, BUTTON_HEIGHT, IMAGE_WIDTH, IMAGE_HEIGHT,
            sizeof(TBBUTTON));
}

void Toolbar_AddDrawButtons()
{
    // define new buttons
	TBBUTTON tbButtons[] =
	{		
		{ 0,        	ID_DRAW_LINE,           TBSTATE_ENABLED,    TBSTYLE_BUTTON, 0,  0 },
		{ 1,        	ID_DRAW_RECTANGLE,	    TBSTATE_ENABLED,    TBSTYLE_BUTTON, 0,  0 },
		{ 2,        	ID_DRAW_ELLIPSE,	    TBSTATE_ENABLED,    TBSTYLE_BUTTON, 0,  0 },
		{ 3,        	ID_DRAW_TEXT,	        TBSTATE_ENABLED,    TBSTYLE_BUTTON, 0,  0 },
		{ 4,        	ID_DRAW_SELECTOBJECT,	TBSTATE_ENABLED,    TBSTYLE_BUTTON, 0,  0 }        
	};
	
	// Structure contains the bitmap of user defined buttons. It contains 5 icons
	TBADDBITMAP	tbBitmap = { hInst, IDB_DRAWBUTTONS };

	// Add bitmap to Image-list of ToolBar
	int idx = SendMessage(hToolbarWnd, TB_ADDBITMAP, (WPARAM) sizeof(tbBitmap) / sizeof(TBADDBITMAP),
		(LPARAM)(LPTBADDBITMAP) &tbBitmap);

	// identify the bitmap index of each button
    for (int i = 0; i < 5; i++) {
        tbButtons[i].iBitmap += idx;
    }

	// add buttons to toolbar
	SendMessage(hToolbarWnd, TB_ADDBUTTONS, (WPARAM) sizeof(tbButtons) / sizeof(TBBUTTON),
		(LPARAM)(LPTBBUTTON) &tbButtons);
    DrawButtonsAreDrawn = true;
}

void Toolbar_AddEditButtons()
{
    // define new buttons
	TBBUTTON tbButtons[] =
	{	
        { 0,                0,                      TBSTATE_ENABLED,    TBSTYLE_SEP,    0,  0 },
		{ STD_CUT,        	ID_EDIT_CUT,            TBSTATE_ENABLED,    TBSTYLE_BUTTON, 0,  0 },
		{ STD_COPY,        	ID_EDIT_COPY,   	    TBSTATE_ENABLED,    TBSTYLE_BUTTON, 0,  0 },
		{ STD_PASTE,        ID_EDIT_PASTE,  	    TBSTATE_ENABLED,    TBSTYLE_BUTTON, 0,  0 },
		{ STD_DELETE,       ID_EDIT_DELETE,	        TBSTATE_ENABLED,    TBSTYLE_BUTTON, 0,  0 }
	};	

	// add buttons to toolbar
	SendMessage(hToolbarWnd, TB_ADDBUTTONS, (WPARAM) sizeof(tbButtons) / sizeof(TBBUTTON),
		(LPARAM)(LPTBBUTTON) &tbButtons);
    EditButtonsAreDrawn = true;
}

void Toolbar_RemoveDrawButtons()
{
    for (int i = 4; i < 9; i++) {
        SendMessage(hToolbarWnd, TB_DELETEBUTTON, (WPARAM) i, (LPARAM) 0);
    }
}

void Toolbar_NotifyHandle(LPARAM lParam)
{
	LPTOOLTIPTEXT   lpToolTipText;
	WCHAR			szToolTipText[TOOL_TIP_MAX_LEN]; 	// ToolTipText, loaded from Stringtable resource

	// lParam: address of TOOLTIPTEXT struct
	lpToolTipText = (LPTOOLTIPTEXT)lParam;

	if (lpToolTipText->hdr.code == TTN_NEEDTEXT)
	{
		// hdr.iFrom: ID cua ToolBar button -> ID cua ToolTipText string
		LoadString(hInst, lpToolTipText->hdr.idFrom, szToolTipText, TOOL_TIP_MAX_LEN);

		lpToolTipText->lpszText = szToolTipText;
	}
}

void ShowColorDialog(HWND hWnd)
{
	COLORREF acrCustClr[16];        // custom colors
	DWORD rgbCurrent = RGB(255, 255, 255);

    CHOOSECOLOR cc;
	ZeroMemory(&cc, sizeof(CHOOSECOLOR));

	cc.lStructSize = sizeof(CHOOSECOLOR);
	cc.hwndOwner = hWnd;
	cc.lpCustColors = (LPDWORD)acrCustClr;
	cc.rgbResult = rgbCurrent;
	cc.Flags = CC_FULLOPEN | CC_RGBINIT;

	if (ChooseColorW(&cc))
	{
        // Get current active child window
        HWND hActiveChild = (HWND) SendMessage(hWndMDIClient, WM_MDIGETACTIVE, 0, 0);
		// Save current color to window data
        MDI_CHILD_DATA* _WndData = (MDI_CHILD_DATA*) GetWindowLongPtrW(hActiveChild, 0);
        _WndData->_color = cc.rgbResult;
    
        /* Apply chosen color to drawing tools with the snippet below
		 * 
		 * MDI_CHILD_DATA* _WndData = (MDI_CHILD_DATA*) GetWindowLongPtrW(hCurrentWnd, 0);
         * HDC hdc = GetDC(hCurrentWnd);
		 * SelectObject(hdc, GetStockObject(DC_PEN));
		 * SelectObject(hdc, GetStockObject(DC_BRUSH));
		 * SetDCPenColor(hdc, _WndData->_color);
         * SetDCBrushColor(hdc, _WndData->_color);
         * ReleaseDC(hActiveChild, hdc); */
	}
	else
	{
        // TODO: Handle exceptions for invalid colors
	}
}

void ShowFontDialog(HWND hWnd)
{
	CHOOSEFONT cf;
	LOGFONT lf;
	HFONT hfNew, hfOld;

	ZeroMemory(&cf, sizeof(CHOOSEFONT));
	cf.lStructSize = sizeof(CHOOSEFONT);
	cf.hwndOwner = hWnd;
	cf.lpLogFont = &lf;
	cf.Flags = CF_SCREENFONTS | CF_EFFECTS;

	if (ChooseFontW(&cf))
	{
        // Get current active child window
        HWND hActiveChild = (HWND) SendMessage(hWndMDIClient, WM_MDIGETACTIVE, 0, 0);
        // Save current font to window data
        MDI_CHILD_DATA* _WndData = (MDI_CHILD_DATA*) GetWindowLongPtrW(hActiveChild, 0);
        _WndData->_font = *cf.lpLogFont;
		_WndData->_color = cf.rgbColors;

        /* Apply chosen font to text tools with the snippet below
		 *
		 * MDI_CHILD_DATA* _WndData = (MDI_CHILD_DATA*) GetWindowLongPtrW(hCurrentWnd, 0);
         * HDC hdc = GetDC(hCurrentWnd);
         * hfNew = CreateFontIndirect(_WndData->_font);
         * hfOld = (HFONT) SelectObject(hdc, hfNew);
         * ReleaseDC(hActiveChild, hdc); */
	}
    else
    {
        // TODO: Handle exceptions for invalid fonts
    }
}

void ShowOpenDialog(HWND hWnd) {
    OPENFILENAME ofn;
    TCHAR szFile[256];
    TCHAR szFilter[] = TEXT("Drawing file(*.drw)\0*.drw\0");
    szFile[0] = '\0';
    ZeroMemory(&ofn, sizeof(OPENFILENAME));
    ofn.lStructSize = sizeof(OPENFILENAME);
    ofn.hwndOwner = hWnd;
    ofn.lpstrFilter = szFilter;
    ofn.nFilterIndex = 1;
    ofn.lpstrFile = szFile;
    ofn.nMaxFile = sizeof(szFile);
    ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
    if (GetOpenFileName(&ofn)) {
        // TODO: Open file
        OpenDrawingFile(hWnd, ofn.lpstrFile);
    }
    else {
        // TODO: Handle errors
    }
}

void ShowSaveDialog(HWND hWnd) {
    OPENFILENAME ofn;
    TCHAR szFile[256];
    TCHAR szFilter[] = TEXT("Drawing file(*.drw)\0*.drw\0");
    szFile[0] = '\0';
    ZeroMemory(&ofn, sizeof(OPENFILENAME));
    ofn.lStructSize = sizeof(OPENFILENAME);
    ofn.hwndOwner = hWnd;
    ofn.lpstrFilter = szFilter;
    ofn.nFilterIndex = 0;
    ofn.lpstrFile = szFile;
    ofn.nMaxFile = sizeof(szFile);
    ofn.Flags = OFN_PATHMUSTEXIST;
    if (GetSaveFileName(&ofn)) {
        // TODO: Save file
        SaveDrawingFile(hWnd, ofn.lpstrFile);
    }
    else {
        // TODO: Handle errors
    }
}

INT_PTR CALLBACK InputText(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam) {
    WCHAR* buf = new WCHAR[100];
    UNREFERENCED_PARAMETER(lParam);
    switch (message) {
    case WM_INITDIALOG:
        {
            // Set focus on textbox
            SetFocus(GetDlgItem(hDlg, IDC_INPUTTEXT));            
        }
        break;
    case WM_COMMAND:
        {
            // Enable OK button only if user has entered something in textbox
            if (GetDlgItemTextW(hDlg, IDC_INPUTTEXT, buf, MAX_LOADSTRING) == NULL) { // No text in textbox
                EnableWindow(GetDlgItem(hDlg, IDOK), FALSE); // Disable OK button
            }
            else {
                EnableWindow(GetDlgItem(hDlg, IDOK), TRUE); // Enable OK button
            }

            // Calculate remaining characters
            int usedChars = wcslen(buf);
            int remChars = MAX_LOADSTRING - usedChars;
            WCHAR wRemChars[4];
            wsprintf(wRemChars, L"%d", remChars);
            SetWindowTextW(GetDlgItem(hDlg, IDC_NUMCHARLEFT), wRemChars);

            // If OK is pressed, pass text in textbox to current TextObj
			// then end dialog
            if (LOWORD(wParam) == IDOK)
            {
                EndDialog(hDlg, (INT_PTR)buf);				
            }

            // If close is pressed, end dialog
            if (LOWORD(wParam) == IDCANCEL)
            {
                EndDialog(hDlg, IDCLOSE);                
            }
        }
		return (INT_PTR)TRUE;
    }
    return (INT_PTR)FALSE;
}

BOOL SaveDrawingFile(HWND hWnd, LPCTSTR lpFileName) {
    std::fstream f;
    bool bSuccess = FALSE;
    
    f.open(lpFileName, std::ios::out | std::ios::binary);
    MDI_CHILD_DATA* _WndData = (MDI_CHILD_DATA*) GetWindowLongPtrW(hWnd, 0);
    f.write((char*)&_WndData->hWnd, sizeof(_WndData->hWnd));
	f.seekp(std::ios_base::end);
    f.write((char*)&_WndData->_color, sizeof(_WndData->_color));
	f.seekp(std::ios_base::end);
    f.write((char*)&_WndData->_font.lfHeight, sizeof(_WndData->_font.lfHeight));
	f.seekp(std::ios_base::end);
    f.write((char*)&_WndData->_font.lfWidth, sizeof(_WndData->_font.lfWidth));
	f.seekp(std::ios_base::end);
    f.write((char*)&_WndData->_font.lfEscapement, sizeof(_WndData->_font.lfEscapement));
	f.seekp(std::ios_base::end);
    f.write((char*)&_WndData->_font.lfOrientation, sizeof(_WndData->_font.lfOrientation));
	f.seekp(std::ios_base::end);
    f.write((char*)&_WndData->_font.lfWeight, sizeof(_WndData->_font.lfWeight));
	f.seekp(std::ios_base::end);
    f.write((char*)&_WndData->_font.lfItalic, sizeof(_WndData->_font.lfItalic));
	f.seekp(std::ios_base::end);
    f.write((char*)&_WndData->_font.lfUnderline, sizeof(_WndData->_font.lfUnderline));
	f.seekp(std::ios_base::end);
    f.write((char*)&_WndData->_font.lfStrikeOut, sizeof(_WndData->_font.lfStrikeOut));
	f.seekp(std::ios_base::end);
    f.write((char*)&_WndData->_font.lfCharSet, sizeof(_WndData->_font.lfCharSet));
	f.seekp(std::ios_base::end);
    f.write((char*)&_WndData->_font.lfOutPrecision, sizeof(_WndData->_font.lfOutPrecision));
	f.seekp(std::ios_base::end);
    f.write((char*)&_WndData->_font.lfClipPrecision, sizeof(_WndData->_font.lfClipPrecision));
	f.seekp(std::ios_base::end);
    f.write((char*)&_WndData->_font.lfQuality, sizeof(_WndData->_font.lfQuality));
	f.seekp(std::ios_base::end);
    f.write((char*)&_WndData->_font.lfPitchAndFamily, sizeof(_WndData->_font.lfPitchAndFamily));
	f.seekp(std::ios_base::end);
    f.write((char*)&_WndData->_font.lfFaceName, sizeof(_WndData->_font.lfFaceName));
	f.seekp(std::ios_base::end);
    f.write((char*)&_WndData->crsType, sizeof(_WndData->crsType));
	f.seekp(std::ios_base::end);
    f.write((char*)&_WndData->DrawCurrTextBox, sizeof(_WndData->DrawCurrTextBox));
	f.seekp(std::ios_base::end);
    f.write((char*)&_WndData->nShapes, sizeof(_WndData->nShapes));
	f.seekp(std::ios_base::end);
    for (int i = 0; i < _WndData->nShapes; i++) {
        _WndData->ShapeArr[i]->WriteToFile(f);
    }

    f.close();

    return bSuccess;
}

BOOL OpenDrawingFile(HWND hWnd, LPCTSTR lpFileName) {
	std::fstream f;
	bool bSuccess = FALSE;

    f.open(lpFileName, std::ios::in | std::ios::binary);

    // Initialize window data
    MDI_CHILD_DATA* _WndData =
        (MDI_CHILD_DATA*) VirtualAlloc(NULL, sizeof(MDI_CHILD_DATA),
        MEM_COMMIT, PAGE_READWRITE);

    f.seekg(0);

    f.read((char*)&_WndData->hWnd, sizeof(_WndData->hWnd));
    f.read((char*)&_WndData->_color, sizeof(_WndData->_color));
    f.read((char*)&_WndData->_font.lfHeight, sizeof(_WndData->_font.lfHeight));
    f.read((char*)&_WndData->_font.lfWidth, sizeof(_WndData->_font.lfWidth));
    f.read((char*)&_WndData->_font.lfEscapement, sizeof(_WndData->_font.lfEscapement));
    f.read((char*)&_WndData->_font.lfOrientation, sizeof(_WndData->_font.lfOrientation));
    f.read((char*)&_WndData->_font.lfWeight, sizeof(_WndData->_font.lfWeight));
    f.read((char*)&_WndData->_font.lfItalic, sizeof(_WndData->_font.lfItalic));
    f.read((char*)&_WndData->_font.lfUnderline, sizeof(_WndData->_font.lfUnderline));
    f.read((char*)&_WndData->_font.lfStrikeOut, sizeof(_WndData->_font.lfStrikeOut));
    f.read((char*)&_WndData->_font.lfCharSet, sizeof(_WndData->_font.lfCharSet));
    f.read((char*)&_WndData->_font.lfOutPrecision, sizeof(_WndData->_font.lfOutPrecision));
    f.read((char*)&_WndData->_font.lfClipPrecision, sizeof(_WndData->_font.lfClipPrecision));
    f.read((char*)&_WndData->_font.lfQuality, sizeof(_WndData->_font.lfQuality));
    f.read((char*)&_WndData->_font.lfPitchAndFamily, sizeof(_WndData->_font.lfPitchAndFamily));
    f.read((char*)&_WndData->_font.lfFaceName, sizeof(_WndData->_font.lfFaceName));
    f.read((char*)&_WndData->crsType, sizeof(_WndData->crsType));
    f.read((char*)&_WndData->DrawCurrTextBox, sizeof(_WndData->DrawCurrTextBox));
    f.read((char*)&_WndData->nShapes, sizeof(_WndData->nShapes));
    for (int i = 0; i < _WndData->nShapes; i++) {
        ShapeType st;
        f.read((char*)st, sizeof(st));
        switch (st) {
        case sLine:
            _WndData->ShapeArr.push_back((Line*) new Line);
            _WndData->ShapeArr.back()->ReadFromFile(f);
            break;
        case sRect:
            _WndData->ShapeArr.push_back((Rect*) new Rect);
            _WndData->ShapeArr.back()->ReadFromFile(f);
            break;
        case sElps:
            _WndData->ShapeArr.push_back((Elps*) new Elps);
            _WndData->ShapeArr.back()->ReadFromFile(f);
            break;
        case sTextObj:
            _WndData->ShapeArr.push_back((TextObj*) new TextObj);
            _WndData->ShapeArr.back()->ReadFromFile(f);
            break;
        default:
            break;
        }
    }

    f.close();

    SetLastError(0);
    // Save data into window's extra bytes
    if (SetWindowLongPtrW(hWnd, 0, (LONG_PTR) _WndData) == 0) {
        if (GetLastError() != 0) {
            // TODO: Handle SetWindowLongPtrW failure

        }
    }

	return bSuccess;
}

int GetSelectedObject(MDI_CHILD_DATA* _WndData) {
    for (int i = _WndData->nShapes - 1; i >= 0; --i) {						
        if (_WndData->ShapeArr[i]->selected) {
            return i;
        }
    }
    return -1;
}

void onCut(HWND hWnd) {
	onCopy(hWnd);
	onDelete(hWnd);
}

void onCopy(HWND hWnd) {
    HWND hActiveChild = (HWND)SendMessage(hWndMDIClient, WM_MDIGETACTIVE, 0, 0);
    MDI_CHILD_DATA* _WndData = (MDI_CHILD_DATA*)GetWindowLongPtrW(hActiveChild, 0);
    CursorType cType = _WndData->crsType;
    if (cType == SelectObj) {
        int selectedIdx = GetSelectedObject(_WndData);
        if (selectedIdx != -1) {
            UINT cFormat = RegisterClipboardFormatW(L"CF_SHAPE");
            HGLOBAL hGlobal = GlobalAlloc(GHND, sizeof(ShapeData));

            ShapeData* sd = (ShapeData*) GlobalLock(hGlobal);
            Shape* selectedShape = _WndData->ShapeArr[selectedIdx];
            selectedShape->GetPoints(sd->left, sd->top, sd->right, sd->bottom);
            sd->color = selectedShape->GetColor();
            sd->font = selectedShape->GetFont();
            sd->sType = selectedShape->GetType();
            if (sd->sType == sTextObj) {
                sd->text = GlobalAlloc(GHND, sizeof(WCHAR)*MAX_LOADSTRING);
                WCHAR* textData = (WCHAR*) GlobalLock(sd->text);
                selectedShape->GetText(textData);
                GlobalUnlock(sd->text);
            }

            GlobalUnlock(hGlobal);

            if (OpenClipboard(hWnd)) {
                EmptyClipboard();
                SetClipboardData(cFormat, hGlobal);
                if (sd->sType == sTextObj) {
                    SetClipboardData(CF_UNICODETEXT, sd->text);
                }
                CloseClipboard();
            }
        }
    }
}

void onPaste(HWND hWnd) {
	HWND hActiveChild = (HWND)SendMessage(hWndMDIClient, WM_MDIGETACTIVE, 0, 0);
    MDI_CHILD_DATA* _WndData = (MDI_CHILD_DATA*)GetWindowLongPtrW(hActiveChild, 0);

    UINT cFormat = RegisterClipboardFormatW(L"CF_SHAPE");
    UINT nFormats[2] = {
        cFormat,
        CF_TEXT
    };

	if (OpenClipboard(hActiveChild)) {
        UINT nFormat = GetPriorityClipboardFormat(nFormats, 2);
        if (nFormat != -1 && nFormat != NULL) { // Found format in priority list
			HANDLE hData = GetClipboardData(nFormat);
			if (hData != NULL) {
				if (nFormat == cFormat) {
					ShapeData* sd = (ShapeData*)GlobalLock(hData);
					switch (sd->sType) {
					case sLine:
						{
							Line* newLine = new Line;
							newLine->SetStartPoint(sd->left, sd->top);
							newLine->SetEndPoint(sd->right, sd->bottom);
							newLine->SetColor(sd->color);
							_WndData->ShapeArr.push_back(newLine);
						}
						break;
					case sRect:
						{
							Rect* newRect = new Rect;
							newRect->SetStartPoint(sd->left, sd->top);
							newRect->SetEndPoint(sd->right, sd->bottom);
							newRect->SetColor(sd->color);
							_WndData->ShapeArr.push_back(newRect);
						}
						break;
					case sElps:
						{
							Elps* newElps = new Elps;
							newElps->SetStartPoint(sd->left, sd->top);
							newElps->SetEndPoint(sd->right, sd->bottom);
							newElps->SetColor(sd->color);
							_WndData->ShapeArr.push_back(newElps);
						}
						break;
					case sTextObj:
						{
							TextObj* newTextObj = new TextObj;
							newTextObj->SetStartPoint(sd->left, sd->top);
							newTextObj->SetEndPoint(sd->right, sd->bottom);
							newTextObj->SetColor(sd->color);
							newTextObj->SetFont(sd->font);

							// Get text from clipboard
							HANDLE hText = GetClipboardData(CF_UNICODETEXT);
							if (hText != NULL) {
								WCHAR* szText;
								WCHAR* pData = (WCHAR*)GlobalLock(hText);
								szText = new WCHAR[wcslen(pData)+1];
								wcscpy(szText, pData);
								newTextObj->SetText(szText);
								delete[] szText;
							}

							_WndData->ShapeArr.push_back(newTextObj);
						}
						break;
					}
					_WndData->nShapes++;
					GlobalUnlock(hData);                    
				}
				else { // CF_TEXT
					char* szText;
					char* pData = (char*)GlobalLock(hData);
					szText = new char[strlen(pData) + 1];
					strcpy(szText, pData);

					// Convert szText to WCHAR*
					WCHAR* szwText;
					int nChars = MultiByteToWideChar(CP_ACP, 0, szText, -1, NULL, 0);
					szwText = new WCHAR[nChars];
					MultiByteToWideChar(CP_ACP, 0, szText, -1, (LPWSTR)szwText, nChars);

					// Create new object
					RECT r;
					GetClientRect(hActiveChild, &r);
					TextObj* newObj = new TextObj;
					newObj->SetText(szwText);
					newObj->SetStartPoint(r.left, r.top);
					newObj->SetEndPoint(r.right, r.bottom);
					newObj->SetColor(_WndData->_color);
					newObj->SetFont(_WndData->_font);
					_WndData->ShapeArr.push_back(newObj);
					_WndData->nShapes++;

					// Clean up
					delete[] szText;
					delete[] szwText;
				}
			}			
        }
    }
	CloseClipboard();

	InvalidateRect(hActiveChild, nullptr, FALSE);
}

void onDelete(HWND hWnd) {
    HWND hActiveChild = (HWND)SendMessage(hWndMDIClient, WM_MDIGETACTIVE, 0, 0);
    MDI_CHILD_DATA* _WndData = (MDI_CHILD_DATA*)GetWindowLongPtrW(hActiveChild, 0);
    CursorType cType = _WndData->crsType;
    if (cType == SelectObj) {
		int selectedIdx = GetSelectedObject(_WndData);

		if (selectedIdx != -1) {
			// Create temporary vector and delete selected object there
			std::vector<Shape*> tmp = _WndData->ShapeArr;


			/*tmp.erase(std::remove_if(tmp.begin(),
									tmp.end(),
									[&](const Shape* shape)-> bool
										{ return shape->selected; }),
						tmp.end());*/

			auto it = tmp.begin() + selectedIdx;
			it = tmp.erase(it);

			// Swap temporary vector with storage vector
			std::swap(tmp, _WndData->ShapeArr);
			_WndData->nShapes--;

			// Clean up
			tmp.clear();
		}
    }
	
    InvalidateRect(hActiveChild, nullptr, FALSE);
}