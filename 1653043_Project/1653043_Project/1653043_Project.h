#pragma comment(lib, "comctl32.lib")

#include "resource.h"
#include "myShapes.h"
#include <CommCtrl.h>				    // Used for toolbar functions
#include <CommDlg.h>                    // Used for 'font' and 'color' dialog boxes
#include <vector>
#include <memory>
#include <fstream>
#include <algorithm>
#include <functional>

enum CursorType {
    Normal,
    DrawLine,
    DrawRect,
    DrawElps,
    DrawTextObj,
    SelectObj,
    nCursorType
};

struct MDI_CHILD_DATA {
    HWND hWnd;
    COLORREF _color;
    LOGFONT _font;
    CursorType crsType;
	std::vector<Shape*> ShapeArr;
	int nShapes;
	bool DrawCurrTextBox;
};
