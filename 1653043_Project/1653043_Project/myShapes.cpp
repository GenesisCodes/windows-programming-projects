#include "stdafx.h"
#include "myShapes.h"


double getDistance(int x1, int y1, int x2, int y2) {
	return (sqrt(1.0*(x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1)));
}

void Shape::Move(int distX, int distY) {
	left += distX; right += distX;
	top += distY; bottom += distY;
}

void Line::Draw(HDC hdc) {
    SelectObject(hdc, GetStockObject(DC_PEN));
    SelectObject(hdc, GetStockObject(DC_BRUSH));
    SetDCPenColor(hdc, color);
    SetDCBrushColor(hdc, color);

    MoveToEx(hdc, left, top, nullptr);
    LineTo(hdc, right, bottom);
}

void Rect::Draw(HDC hdc) {
    SelectObject(hdc, GetStockObject(DC_PEN));
    SelectObject(hdc, GetStockObject(DC_BRUSH));
    SetDCPenColor(hdc, color);
    SetDCBrushColor(hdc, color);

    Rectangle(hdc, left, top, right, bottom);
}

void Elps::Draw(HDC hdc) {
    SelectObject(hdc, GetStockObject(DC_PEN));
    SelectObject(hdc, GetStockObject(DC_BRUSH));
    SetDCPenColor(hdc, color);
    SetDCBrushColor(hdc, color);

    Ellipse(hdc, left, top, right, bottom);
}

void TextObj::Draw(HDC hdc) {
    SetTextColor(hdc, color);

	HFONT hf = CreateFontIndirectW(&font);
    HFONT hfOld = (HFONT) SelectObject(hdc, hf);
	
    if (left > right) {
        int tmp = left;
        left = right;
        right = tmp;
    }
    if (top > bottom) {
        int tmp = top;
        top = bottom;
        bottom = tmp;
    }
    
    RECT r = {left, top, right, bottom};
    DrawTextW(hdc, text, wcslen(text), &r, DT_CALCRECT | DT_WORDBREAK);
	SetEndPoint(r.right, r.bottom);
	DrawTextW(hdc, text, wcslen(text), &r, DT_WORDBREAK);

	DeleteObject(hf);
	SelectObject(hdc, hfOld);
}

void TextObj::DrawTextBox(HDC hdc) {
    HPEN hPen = CreatePen(PS_DOT, 0, RGB(0, 0, 0));
    SelectObject(hdc, hPen);
    SelectObject(hdc, GetStockObject(NULL_BRUSH));
    Rectangle(hdc, left, top, right, bottom);
	DeleteObject(hPen);
}

bool Line::mouseOver(int mX, int mY) {
	return (getDistance(mX, mY, left, top) + getDistance(mX, mY, right, bottom) >= getDistance(left, top, right, bottom) - 0.1 &&
		(getDistance(mX, mY, left, top) + getDistance(mX, mY, right, bottom) <= getDistance(left, top, right, bottom) + 0.1));
}

bool Rect::mouseOver(int mX, int mY) {
    return ((mX > left && mX < right) || (mX > right && mX < left)) &&
           ((mY > top && mY < bottom) || (mY > bottom && mY < top));
}

bool Elps::mouseOver(int mX, int mY) {
    return ((mX > left && mX < right) || (mX > right && mX < left)) &&
           ((mY > top && mY < bottom) || (mY > bottom && mY < top));
}

bool TextObj::mouseOver(int mX, int mY) {
    return ((mX > left && mX < right) || (mX > right && mX < left)) &&
           ((mY > top && mY < bottom) || (mY > bottom && mY < top));
}

void Line::cueSelection(HDC hdc) {
	SetROP2(hdc, R2_NOTXORPEN);
	MoveToEx(hdc, left, top, nullptr);
	LineTo(hdc, right, bottom);

	HPEN hPen = CreatePen(PS_DOT, 0, RGB(0, 0, 0));
	SelectObject(hdc, hPen);
	MoveToEx(hdc, left, top, nullptr);
	LineTo(hdc, right, bottom);
	DeleteObject(hPen);
}

void Rect::cueSelection(HDC hdc) {
	HPEN hPen = CreatePen(PS_DOT, 0, RGB(0, 0, 0));
	SelectObject(hdc, hPen);
	SelectObject(hdc, GetStockObject(NULL_BRUSH));
	Rectangle(hdc, left, top, right, bottom);
	DeleteObject(hPen);
}

void Elps::cueSelection(HDC hdc) {
	HPEN hPen = CreatePen(PS_DOT, 0, RGB(0, 0, 0));
	SelectObject(hdc, hPen);
	SelectObject(hdc, GetStockObject(NULL_BRUSH));
	Rectangle(hdc, left, top, right, bottom);
	DeleteObject(hPen);
}

void TextObj::cueSelection(HDC hdc) {
	HPEN hPen = CreatePen(PS_DOT, 0, RGB(0, 0, 0));
	SelectObject(hdc, hPen);
	SelectObject(hdc, GetStockObject(NULL_BRUSH));
	Rectangle(hdc, left, top, right, bottom);
	DeleteObject(hPen);
}

void Line::displaySelection(HDC hdc) {
	SelectObject(hdc, GetStockObject(BLACK_PEN));
	SelectObject(hdc, GetStockObject(WHITE_BRUSH));
	POINT AnchorPoints[2]; GetAnchorPoints(AnchorPoints);
	for (int i = 0; i < 2; i++) {
		Ellipse(hdc, AnchorPoints[i].x - 4, AnchorPoints[i].y - 4, AnchorPoints[i].x + 4, AnchorPoints[i].y + 4);
	}
}

void Rect::displaySelection(HDC hdc) {
	cueSelection(hdc);
	SelectObject(hdc, GetStockObject(BLACK_PEN));
	SelectObject(hdc, GetStockObject(WHITE_BRUSH));
	POINT AnchorPoints[8]; GetAnchorPoints(AnchorPoints);
	for (int i = 0; i < 8; i++) {
		Ellipse(hdc, AnchorPoints[i].x - 4, AnchorPoints[i].y - 4, AnchorPoints[i].x + 4, AnchorPoints[i].y + 4);
	}
}

void Elps::displaySelection(HDC hdc) {
	cueSelection(hdc);
	SelectObject(hdc, GetStockObject(BLACK_PEN));
	SelectObject(hdc, GetStockObject(WHITE_BRUSH));
    int MidX = (left + right) / 2;
    int MidY = (top + bottom) / 2;
	POINT AnchorPoints[8]; GetAnchorPoints(AnchorPoints);
	for (int i = 0; i < 8; i++) {
		Ellipse(hdc, AnchorPoints[i].x - 4, AnchorPoints[i].y - 4, AnchorPoints[i].x + 4, AnchorPoints[i].y + 4);
	}
}

void TextObj::displaySelection(HDC hdc) {
	cueSelection(hdc);
    /*
	SelectObject(hdc, GetStockObject(BLACK_PEN));
	SelectObject(hdc, GetStockObject(WHITE_BRUSH));
	POINT AnchorPoints[8]; GetAnchorPoints(AnchorPoints);
	for (int i = 0; i < 8; i++) {
		Ellipse(hdc, AnchorPoints[i].x - 4, AnchorPoints[i].y - 4, AnchorPoints[i].x + 4, AnchorPoints[i].y + 4);
	}*/
}

int Line::onAnchor(int x, int y) {
    POINT AnchorPoints[2]; GetAnchorPoints(AnchorPoints);
    for (int i = 0; i < 2; i++) {
        if (x > AnchorPoints[i].x - 4 && x < AnchorPoints[i].x + 4 &&
            y > AnchorPoints[i].y - 4 && y < AnchorPoints[i].y + 4)
                return i;
    }
    return -1;
}

int Rect::onAnchor(int x, int y) {
    POINT AnchorPoints[8]; GetAnchorPoints(AnchorPoints);
    for (int i = 0; i < 8; i++) {
        if (x > AnchorPoints[i].x - 4 && x < AnchorPoints[i].x + 4 &&
            y > AnchorPoints[i].y - 4 && y < AnchorPoints[i].y + 4)
                return i;
    }
    return -1;
}

int Elps::onAnchor(int x, int y) {
    POINT AnchorPoints[8]; GetAnchorPoints(AnchorPoints);
    for (int i = 0; i < 8; i++) {
        if (x > AnchorPoints[i].x - 4 && x < AnchorPoints[i].x + 4 &&
            y > AnchorPoints[i].y - 4 && y < AnchorPoints[i].y + 4)
                return i;
    }
    return -1;
}

void Line::Resize(int distX, int distY, int AnchorIdx) {
    switch(AnchorIdx) {
    case 0:
        left += distX;
        top += distY;
        break;
    case 1:
        right += distX;
        bottom += distY;
        break;
    }
}

void Rect::Resize(int distX, int distY, int AnchorIdx) {
    switch (AnchorIdx) {
    case 0: // Top left
        left += distX;
        top += distY;
        break;
    case 1: // Top middle
        top += distY;
        break;
    case 2: // Top right
        right += distX;
        top += distY;
        break;
    case 3: // Middle left
        left += distX;
        break;
    case 4: // Middle right
        right += distX;
        break;
    case 5: // Bottom left
        left += distX;
        bottom += distY;
        break;
    case 6: // Bottom middle
        bottom += distY;
        break;
    case 7: // Bottom right
        right += distX;
        bottom += distY;
        break;
    }
}

void Elps::Resize(int distX, int distY, int AnchorIdx) {
    switch (AnchorIdx) {
    case 0: // Top left
        left += distX;
        top += distY;
        break;
    case 1: // Top middle
        top += distY;
        break;
    case 2: // Top right
        right += distX;
        top += distY;
        break;
    case 3: // Middle left
        left += distX;
        break;
    case 4: // Middle right
        right += distX;
        break;
    case 5: // Bottom left
        left += distX;
        bottom += distY;
        break;
    case 6: // Bottom middle
        bottom += distY;
        break;
    case 7: // Bottom right
        right += distX;
        bottom += distY;
        break;
    }
}

void Line::WriteToFile(std::fstream& f) {
    f.write((char*)&sType, sizeof(sType));
	f.seekp(std::ios_base::end);
    f.write((char*)&left, sizeof(left));
	f.seekp(std::ios_base::end);
    f.write((char*)&top, sizeof(top));
	f.seekp(std::ios_base::end);
    f.write((char*)&right, sizeof(right));
	f.seekp(std::ios_base::end);
    f.write((char*)&bottom, sizeof(bottom));
	f.seekp(std::ios_base::end);
    f.write((char*)&color, sizeof(color));
	f.seekp(std::ios_base::end);
}

void Rect::WriteToFile(std::fstream& f) {
    f.write((char*)&sType, sizeof(sType));
	f.seekp(std::ios_base::end);
    f.write((char*)&left, sizeof(left));
	f.seekp(std::ios_base::end);
    f.write((char*)&top, sizeof(top));
	f.seekp(std::ios_base::end);
    f.write((char*)&right, sizeof(right));
	f.seekp(std::ios_base::end);
	f.write((char*)&bottom, sizeof(bottom));
	f.seekp(std::ios_base::end);
    f.write((char*)&color, sizeof(color));
	f.seekp(std::ios_base::end);
}

void Elps::WriteToFile(std::fstream& f) {
    f.write((char*)&sType, sizeof(sType));
	f.seekp(std::ios_base::end);
    f.write((char*)&left, sizeof(left));
	f.seekp(std::ios_base::end);
    f.write((char*)&top, sizeof(top));
	f.seekp(std::ios_base::end);
    f.write((char*)&right, sizeof(right));
	f.seekp(std::ios_base::end);
    f.write((char*)&bottom, sizeof(bottom));
	f.seekp(std::ios_base::end);
    f.write((char*)&color, sizeof(color));
	f.seekp(std::ios_base::end);
}

void TextObj::WriteToFile(std::fstream& f) {
    f.write((char*)&sType, sizeof(sType));
	f.seekp(std::ios_base::end);
    f.write((char*)&left, sizeof(left));
	f.seekp(std::ios_base::end);
    f.write((char*)&top, sizeof(top));
	f.seekp(std::ios_base::end);
    f.write((char*)&right, sizeof(right));
	f.seekp(std::ios_base::end);
    f.write((char*)&bottom, sizeof(bottom));
	f.seekp(std::ios_base::end);
    f.write((char*)&color, sizeof(color));
	f.seekp(std::ios_base::end);
    f.write((char*)&font.lfHeight, sizeof(font.lfHeight));
	f.seekp(std::ios_base::end);
    f.write((char*)&font.lfWidth, sizeof(font.lfWidth));
	f.seekp(std::ios_base::end);
    f.write((char*)&font.lfEscapement, sizeof(font.lfEscapement));
	f.seekp(std::ios_base::end);
    f.write((char*)&font.lfOrientation, sizeof(font.lfOrientation));
	f.seekp(std::ios_base::end);
    f.write((char*)&font.lfWeight, sizeof(font.lfWeight));
	f.seekp(std::ios_base::end);
    f.write((char*)&font.lfItalic, sizeof(font.lfItalic));
	f.seekp(std::ios_base::end);
    f.write((char*)&font.lfUnderline, sizeof(font.lfUnderline));
	f.seekp(std::ios_base::end);
    f.write((char*)&font.lfStrikeOut, sizeof(font.lfStrikeOut));
	f.seekp(std::ios_base::end);
    f.write((char*)&font.lfCharSet, sizeof(font.lfCharSet));
	f.seekp(std::ios_base::end);
    f.write((char*)&font.lfOutPrecision, sizeof(font.lfOutPrecision));
	f.seekp(std::ios_base::end);
    f.write((char*)&font.lfClipPrecision, sizeof(font.lfClipPrecision));
	f.seekp(std::ios_base::end);
    f.write((char*)&font.lfQuality, sizeof(font.lfQuality));
	f.seekp(std::ios_base::end);
    f.write((char*)&font.lfPitchAndFamily, sizeof(font.lfPitchAndFamily));
	f.seekp(std::ios_base::end);
    f.write((char*)&font.lfFaceName, sizeof(font.lfFaceName));
	f.seekp(std::ios_base::end);
    f.write((char*)&text, sizeof(text));
	f.seekp(std::ios_base::end);
}

void Line::ReadFromFile(std::fstream& f) {
    f.read((char*)sType, sizeof(sType));
	switch (sType) {
	case sLine: case sRect: case sElps:
		f.read((char*)left, sizeof(left));
		f.read((char*)top, sizeof(top));
		f.read((char*)right, sizeof(right));
		f.read((char*)bottom, sizeof(bottom));
		f.read((char*)color, sizeof(color));
		break;
	default:
		break;
	}
}

void Rect::ReadFromFile(std::fstream& f) {
	f.read((char*)sType, sizeof(sType));
	switch (sType) {
	case sLine: case sRect: case sElps:
		f.read((char*)left, sizeof(left));
		f.read((char*)top, sizeof(top));
		f.read((char*)right, sizeof(right));
		f.read((char*)bottom, sizeof(bottom));
		f.read((char*)color, sizeof(color));
		break;
	default:
		break;
	}
}

void Elps::ReadFromFile(std::fstream& f) {
	f.read((char*)sType, sizeof(sType));
	switch (sType) {
	case sLine: case sRect: case sElps:
		f.read((char*)left, sizeof(left));
		f.read((char*)top, sizeof(top));
		f.read((char*)right, sizeof(right));
		f.read((char*)bottom, sizeof(bottom));
		f.read((char*)color, sizeof(color));
		break;
	default:
		break;
	}
}

void TextObj::ReadFromFile(std::fstream& f) {
	f.read((char*)sType, sizeof(sType));
	f.read((char*)left, sizeof(left));
	f.read((char*)top, sizeof(top));
	f.read((char*)right, sizeof(right));
	f.read((char*)bottom, sizeof(bottom));
	f.read((char*)color, sizeof(color));
	f.read((char*)font.lfHeight, sizeof(font.lfHeight));
	f.read((char*)font.lfWidth, sizeof(font.lfWidth));
	f.read((char*)font.lfEscapement, sizeof(font.lfEscapement));
	f.read((char*)font.lfOrientation, sizeof(font.lfOrientation));
	f.read((char*)font.lfWeight, sizeof(font.lfWeight));
	f.read((char*)font.lfItalic, sizeof(font.lfItalic));
	f.read((char*)font.lfUnderline, sizeof(font.lfUnderline));
	f.read((char*)font.lfStrikeOut, sizeof(font.lfStrikeOut));
	f.read((char*)font.lfCharSet, sizeof(font.lfCharSet));
	f.read((char*)font.lfOutPrecision, sizeof(font.lfOutPrecision));
	f.read((char*)font.lfClipPrecision, sizeof(font.lfClipPrecision));
	f.read((char*)font.lfQuality, sizeof(font.lfQuality));
	f.read((char*)font.lfPitchAndFamily, sizeof(font.lfPitchAndFamily));
	f.read((char*)font.lfFaceName, sizeof(font.lfFaceName));
	f.read((char*)text, sizeof(text));
}