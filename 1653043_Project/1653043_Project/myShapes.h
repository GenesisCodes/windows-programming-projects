#pragma once

#include <math.h>
#include <fstream>

#define MAX_LOADSTRING 100

enum ShapeType {
	sLine,
	sRect,
	sElps,
	sTextObj,
	nShapeType
};

class Shape {
public:
	virtual void Draw(HDC) { };
	virtual void SetFont(LOGFONT) { };
	virtual bool mouseOver(int, int) { return FALSE; };
	virtual void DrawTextBox(HDC) { };
    virtual void SetText(WCHAR[]) { };
	virtual void cueSelection(HDC) { };
    virtual void displaySelection(HDC) { };
    virtual POINT* GetAnchorPoints() { return NULL; };
	virtual int onAnchor(int, int) { return -1; };
    virtual void Resize(int, int, int) { };
	virtual void WriteToFile(std::fstream&) { };
	virtual void ReadFromFile(std::fstream&) { };
	virtual ShapeType GetType() { return sType; };
	void Move(int, int);
    void SetStartPoint(int left, int top) {
        this->top = top;
        this->left = left;
    }
    void SetEndPoint(int right, int bottom) {
        this->bottom = bottom;
        this->right = right;
    }
    void SetColor(COLORREF color) {
        this->color = color;
    }
	virtual void GetPoints(int& left, int& top, int& right, int& bottom) {
		return;
	}
	virtual COLORREF GetColor() {
		return this->color;
	}
	virtual LOGFONT GetFont() {
		return this->font;
	}
	virtual void GetText(WCHAR* ws) {
		return;
	}
	bool selected = FALSE;
	bool hoverOn = FALSE;	
protected:
	ShapeType sType = nShapeType;
    int top, left, bottom, right;
    COLORREF color = NULL;
	LOGFONT font;
	WCHAR* text = NULL;
};

class Line : public Shape {
public:
	void cueSelection(HDC);
	void displaySelection(HDC);
	void Draw(HDC);
	bool mouseOver(int, int);
    int onAnchor(int, int);
    void Resize(int, int, int);
	void WriteToFile(std::fstream&);
	void ReadFromFile(std::fstream&);
	ShapeType GetType() { return sType; };
	void GetPoints(int& left, int& top, int& right, int& bottom) {
		left = this->left;
		top = this->top;
		right = this->right;
		bottom = this->bottom;
	}
	COLORREF GetColor() {
		return this->color;
	}
	LOGFONT GetFont() {
		return this->font;
	}
    Line(int left = NULL, int top = NULL, int right = NULL, int bottom = NULL) {
        this->left = left;
        this->top = top;
        this->right = right;
        this->bottom = bottom;
    };
	void GetAnchorPoints(POINT AnchorPoints[]) {
		POINT AP[2] = {
			{left, top}, {right, bottom}
		};
		for (int i = 0; i < 2; i++) {
			AnchorPoints[i] = AP[i];
		}
	}
protected:
	ShapeType sType = sLine;
};

class Rect : public Shape {
public:
	void Draw(HDC);
	bool mouseOver(int, int);
	void cueSelection(HDC);
	void displaySelection(HDC);
    int onAnchor(int, int);
    void Resize(int, int, int);
	void WriteToFile(std::fstream&);
	void ReadFromFile(std::fstream&);	
	ShapeType GetType() { return sType; };
	void GetPoints(int& left, int& top, int& right, int& bottom) {
		left = this->left;
		top = this->top;
		right = this->right;
		bottom = this->bottom;
	}
	COLORREF GetColor() {
		return this->color;
	}
	LOGFONT GetFont() {
		return this->font;
	}
	Rect(int left = NULL, int top = NULL, int right = NULL, int bottom = NULL) {
        this->left = left;
        this->top = top;
        this->right = right;
        this->bottom = bottom;
    };    
	void GetAnchorPoints(POINT AnchorPoints[]) {
		POINT AP[8] = {
			{left, top},	{(left + right) / 2, top},		{right, top},
			{left, (top + bottom) / 2},						{right,(top + bottom) / 2},
			{left, bottom},	{(left + right) / 2,bottom},	{right,bottom}
		};
		for (int i = 0; i < 8; i++) {
			AnchorPoints[i] = AP[i];
		}
	}
protected:
	ShapeType sType = sRect;
};

class Elps : public Shape {
public:
	void Draw(HDC);
	bool mouseOver(int, int);
	void cueSelection(HDC);
	void displaySelection(HDC);
    int onAnchor(int, int);
    void Resize(int, int, int);
	void WriteToFile(std::fstream&);
	void ReadFromFile(std::fstream&);
	ShapeType GetType() { return sType; };
	void GetPoints(int& left, int& top, int& right, int& bottom) {
		left = this->left;
		top = this->top;
		right = this->right;
		bottom = this->bottom;
	}
	COLORREF GetColor() {
		return this->color;
	}
	LOGFONT GetFont() {
		return this->font;
	}
    Elps(int left = NULL, int top = NULL, int right = NULL, int bottom = NULL) {
        this->left = left;
        this->top = top;
        this->right = right;
        this->bottom = bottom;
    };    
	void GetAnchorPoints(POINT AnchorPoints[]) {
		POINT AP[8] = {
			{ left, top },{ (left + right) / 2, top },{ right, top },
			{ left, (top + bottom) / 2 },{ right,(top + bottom) / 2 },
			{ left, bottom },{ (left + right) / 2,bottom },{ right,bottom }
		};
		for (int i = 0; i < 8; i++) {
			AnchorPoints[i] = AP[i];
		}
	}
protected:
	ShapeType sType = sElps;
};

class TextObj : public Shape {
public:
    TextObj() {	};
	void Draw(HDC);
	bool mouseOver(int, int);
	void DrawTextBox(HDC);
	void cueSelection(HDC);
	void displaySelection(HDC);
	void WriteToFile(std::fstream&);
	void ReadFromFile(std::fstream&);
	ShapeType GetType() { return sType; };
	void GetPoints(int& left, int& top, int& right, int& bottom) {
		left = this->left;
		top = this->top;
		right = this->right;
		bottom = this->bottom;
	}
	COLORREF GetColor() {
		return this->color;
	}
	LOGFONT GetFont() {
		return this->font;
	}
	void GetText(WCHAR* ws) {
		if (this->text != NULL) {
			wcscpy(ws, this->text);
		}
	}
    void SetFont(LOGFONT lf) {
        this->font = lf;
    }
    void SetText(WCHAR buf[]) {
        for (int i = 0; i < MAX_LOADSTRING; ++i) {
            this->text[i] = buf[i];
        }
    }
protected:
	ShapeType sType = sTextObj;
    LOGFONT font;
	WCHAR text[MAX_LOADSTRING];
};

struct ShapeData {
	int left, top, right, bottom;
	COLORREF color;
	LOGFONT font;
	HGLOBAL text;
	ShapeType sType;
};